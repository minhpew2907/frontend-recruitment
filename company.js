function showFeature(featureId) {
  // Hide all features
  const features = document.getElementsByClassName("feature");
  for (let i = 0; i < features.length; i++) {
    features[i].classList.remove("active");
  }

  // Show the selected feature
  const selectedFeature = document.getElementById(featureId);
  selectedFeature.classList.add("active");
}


//Checkbox
const statusCheckboxes = document.querySelectorAll('input[type="checkbox"]');
statusCheckboxes.forEach(checkbox => {
    checkbox.addEventListener('change', () => {
        // Here, you can handle the changes made to the checkboxes.
        // For example, you can update the status of the post in your database or perform any other action.
        console.log(`Checkbox status changed: ${checkbox.checked}`);
    });
});

//Resize show post
function adjustDivHeight() {
  const featureDiv = document.getElementById("show-post");
  const table = featureDiv.querySelector("table");
  const tableHeight = table.offsetHeight;
  featureDiv.style.height = `${tableHeight + 120}px`; // Adjust padding as needed
}

// Call the function initially and whenever the window is resized
adjustDivHeight();
window.addEventListener("resize", adjustDivHeight);

function adjustDivShowApplyHeight() {
  const featureDiv = document.getElementById("show-apply");
  const table = featureDiv.querySelector("table");
  const tableHeight = table.offsetHeight;
  featureDiv.style.height = `${tableHeight + 120}px`; // Adjust padding as needed
}

// Call the function initially and whenever the window is resized
adjustDivShowApplyHeight();
window.addEventListener("resize", adjustDivShowApplyHeight);
